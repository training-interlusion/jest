export function generateRandomNumberComplete(min, max) {
  min = +min;
  max = +max;

  if (isNaN(min)) {
    min = 0;
  }

  if (isNaN(max)) {
    max = Number.MAX_SAFE_INTEGER;
  }

  if (min > max) {
    throw new Error('<min> must be smaller than <max>');
  }

  const n = min + Math.random() * (max - min);

  return {
    min,
    max,
    number: n,
  };
}

export function generateRandomNumber(min, max) {
  return generateRandomNumberComplete(min, max).number;
}
