import {generateRandomNumberComplete as sut} from './generate-random-number.fn';

describe('generateRandomNumber', () => {
    it('should work somehow', () => {
        const random = sut(1, 2);
        expect(random.number).toBeGreaterThanOrEqual(1);
        expect(random.number).toBeLessThanOrEqual(2);
        expect(random.min).toEqual(1);
        expect(random.max).toEqual(2);
    });
});
